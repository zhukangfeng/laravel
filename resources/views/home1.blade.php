@extends('app')

@section('content')
<div class="container">  
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">
        <div class="panel-heading">{!! $key !!}</div>

        <div class="panel-body">
          <form action="" method="POST">
            <input name="_method" type="hidden" value="PUT">
            <input type="hidden" name="_token" value="">
            <input type="text" name="title" class="form-control" required="required" value="">
            <br>
            <textarea name="body" rows="10" class="form-control" required="required"></textarea>
            <br>
            <button class="btn btn-lg btn-info">新增 Page</button>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>  
@endsection