<?php namespace App\Http\Controllers\About;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AboutController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return view('about.self');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, Auth::id() $id)
	{
		//
		// if (Auth::user()) {
		// 	$id = Auth::user()->id;
		// 	$data['name'] = Input::get(key, default)
		// }

		$this->validate($request, [
            'name' => 'required|max:255',
            'mail' => 'required|max:255',
            'password' => 'required',
        ]);

        if (User::where('id', Auth::user()->id)->update(Input::except(['_method', '_token']))) {
            return Redirect::to('about/self');
        } else {
            return Redirect::back()->withInput()->withErrors('更新失败！');
        }

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
