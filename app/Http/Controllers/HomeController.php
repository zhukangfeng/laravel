<?php namespace App\Http\Controllers;

use App\Page;
use Carbon\Carbon;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
//		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index($page_num = 50)
	{
        // return view('home')->withPages(Page::orderBy("id", "desc")->where('public_date', '<=', Carbon::now())->take($page_num)->get());
        return view('home')->withPages(Page::orderBy("public_date", "desc")->where('public_date', '<=', Carbon::now())->get());
	}

}
