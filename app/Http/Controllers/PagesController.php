<?php namespace App\Http\Controllers;

use App\Page;
use Auth;
use Carbon\Carbon;

class PagesController extends Controller {



    public function show($id)
    {
        $page = Page::find($id);
        if ($page->public_date <= Carbon::now() || Auth::user() && $page->user_id == Auth::user()->id) {
            return view('pages.show')->withPage($page);
        } else {
            \App::abort(401);
        }
       
    }

}